# virt-tasks is now virt-roles

This project has been renamed. Please go to the new website at [https://virt-roles.github.io/](https://virt-roles.github.io/) or the new git repository at [https://github.com/virt-roles/virt-roles/](https://github.com/virt-roles/virt-roles/).

## License

virt-tasks is licensed under the GNU General Public License 3.0 or any later
version.

For questions about virt-tasks, please email Stefan Hajnoczi \<stefanha@redhat.com\>.
